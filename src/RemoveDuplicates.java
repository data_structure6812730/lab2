public class RemoveDuplicates {
    public static int removeDuplicates(int[] nums) {
        if (nums.length == 0) {
            return 0; // If the array is empty, there are no duplicates.
        }

        int k = 1; // Initialize a variable to keep track of the number of unique elements
        int current = nums[0]; // Initialize a variable to store the current unique element

        // Iterate through the array using a second pointer
        for (int i = 1; i < nums.length; i++) {
            if (nums[i] != current) {
                // If the current element is different from the previous unique element
                nums[k] = nums[i]; // Update the next position with the unique element
                current = nums[i]; // Update the current unique element
                k++; // Increment k to indicate a new unique element
            }
        }

        return k; // Return the number of unique elements
    }

    public static void main(String[] args) {
        int[] nums1 = {1, 1, 2};
        int result1 = removeDuplicates(nums1);
        System.out.println("Output 1: " + result1); // Output should be 2
        System.out.print("Modified Array 1: [");
        for (int i = 0; i < result1; i++) {
            System.out.print(nums1[i]);
            if (i < result1 - 1) {
                System.out.print(", ");
            }
        }
        System.out.println("]");

        int[] nums2 = {0, 0, 1, 1, 1, 2, 2, 3, 3, 4};
        int result2 = removeDuplicates(nums2);
        System.out.println("Output 2: " + result2); // Output should be 5
        System.out.print("Modified Array 2: [");
        for (int i = 0; i < result2; i++) {
            System.out.print(nums2[i]);
            if (i < result2 - 1) {
                System.out.print(", ");
            }
        }
        System.out.println("]");
    }
}
