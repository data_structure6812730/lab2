public class RemoveElement {
    public static int removeElement(int[] nums, int val) {
        int k = 0; // Initialize a variable to keep track of the number of elements not equal to val

        // Iterate through the array using two pointers
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] != val) {
                // If the current element is not equal to val, update nums[k] with the current element
                nums[k] = nums[i];
                k++; // Increment k to indicate the next position for a non-val element
            }
        }

        return k; // Return the number of elements not equal to val
    }

    public static void main(String[] args) {
        int[] nums1 = {3, 2, 2, 3};
        int val1 = 3;
        int result1 = removeElement(nums1, val1);
        System.out.println("Output 1: " + result1); // Output should be 2
        System.out.print("Modified Array 1: [");
        for (int i = 0; i < result1; i++) {
            System.out.print(nums1[i]);
            if (i < result1 - 1) {
                System.out.print(", ");
            }
        }
        System.out.println("]");

        int[] nums2 = {0, 1, 2, 2, 3, 0, 4, 2};
        int val2 = 2;
        int result2 = removeElement(nums2, val2);
        System.out.println("Output 2: " + result2); // Output should be 5
        System.out.print("Modified Array 2: [");
        for (int i = 0; i < result2; i++) {
            System.out.print(nums2[i]);
            if (i < result2 - 1) {
                System.out.print(", ");
            }
        }
        System.out.println("]");
    }
}
