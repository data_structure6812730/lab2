public class ArrayDeletionsLab {

        // Method to delete an element at a specific index
        public static int[] deleteElementByIndex(int[] arr, int index) {
            if (index < 0 || index >= arr.length) {
                // Invalid index, return the original array
                return arr;
            }
    
            int[] updatedArr = new int[arr.length - 1];
            int newIndex = 0;
    
            for (int i = 0; i < arr.length; i++) {
                if (i != index) {
                    updatedArr[newIndex] = arr[i];
                    newIndex++;
                }
            }
    
            return updatedArr;
        }
    
        // Method to delete the first occurrence of a specific value
        public static int[] deleteElementByValue(int[] arr, int value) {
            int index = -1;
    
            // Find the index of the first occurrence of the value
            for (int i = 0; i < arr.length; i++) {
                if (arr[i] == value) {
                    index = i;
                    break;
                }
            }
    
            if (index == -1) {
                // Value not found, return the original array
                return arr;
            }
    
            int[] updatedArr = new int[arr.length - 1];
            int newIndex = 0;
    
            for (int i = 0; i < arr.length; i++) {
                if (i != index) {
                    updatedArr[newIndex] = arr[i];
                    newIndex++;
                }
            }
    
            return updatedArr;
        }
    
        public static void main(String[] args) {
            // Create an integer array and initialize it with some values
            int[] originalArray = {1, 2, 3, 4, 5};
    
            // Test deleteElementByIndex method
            System.out.println("Original Array: " + java.util.Arrays.toString(originalArray));
            int[] newArray1 = deleteElementByIndex(originalArray, 2);
            System.out.println("Array after deleting element at index 2: " + java.util.Arrays.toString(newArray1));
    
            // Test deleteElementByValue method
            int[] newArray2 = deleteElementByValue(originalArray, 4);
            System.out.println("Array after deleting element with value 4: " + java.util.Arrays.toString(newArray2));
    
            // Test with different test cases
            int[] testArray = {10, 20, 30, 20, 40, 50};
            System.out.println("\nOriginal Test Array: " + java.util.Arrays.toString(testArray));
            int[] newArray3 = deleteElementByIndex(testArray, 3);
            System.out.println("Array after deleting element at index 3: " + java.util.Arrays.toString(newArray3));
    
            int[] newArray4 = deleteElementByValue(testArray, 20);
            System.out.println("Array after deleting element with value 20: " + java.util.Arrays.toString(newArray4));
        }
    }
    

